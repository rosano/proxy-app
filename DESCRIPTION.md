App Proxy is a service that lets one publish a public HTTPS URL endpoint for a non-Cloudron hosted application.
When a user visits the public endpoint, App Proxy proxies requests to the hosted application.
