Cloudron Proxy App
==================

This is an app without an actual docker image and thus Cloudron will not create a container for it.
The app is mostly about the reverse proxy config managed through the platform.

Due to that it does not require a `cloudron build` step, but the tooling currently will still ask for a dummy `dockerImage` value to publish it.

For this use:

```
cloudron appstore upload --image cloudron/io.cloudron.builtin.appproxy:20220505-110926-799094b01
```

While the timestamp in the end may or may not be adjusted. Anyways this is not used.
